import React, { Component } from "react";
import Cart from "./Cart";
import { DataShoe } from "./DataShoe";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default class ShoeShop extends Component {
  state = {
    shoeArr: DataShoe,
    detail: DataShoe[0],
    cart: [],
  };
  handelChangeDetailShoe = (shoe) => {
    this.setState({
      detail: shoe,
    });
  };
  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    // TH1 chưa có sản phẩm trong giỏ hàng
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      // sp không có trong giỏ hàng
      let cartItem = { ...shoe, number: 1 };
      cloneCart.push(cartItem);
    } else {
      cloneCart[index].number++;
    }

    // TH2 đã có sản phẩm trong giỏ hàng

    this.setState({ cart: cloneCart });
  };
  render() {
    return (
      <div>
        <Cart cart={this.state.cart} />
        <ListShoe
          handleAddToCart={this.handleAddToCart}
          handelChangeDetailShoe={this.handelChangeDetailShoe}
          shoeArr={this.state.shoeArr}
        />
        <DetailShoe detail={this.state.detail} />
      </div>
    );
  }
}
