import React, { Component } from "react";

export default class DetailShoe extends Component {
  render() {
    let { id, name, description, price, image } = this.props.detail;

    return (
      <div className="row mt-5 alert-secondary text-left">
        <img src={image} alt="" className="col-3" />
        <div className="col-9 m-auto">
          <p>ID : {id}</p>
          <p>Name : {name}</p>
          <p>Desc : {description}</p>
          <p>Price : {price} </p>
        </div>
      </div>
    );
  }
}
