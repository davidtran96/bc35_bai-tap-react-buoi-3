import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { id, image, name, price } = this.props.data;

    return (
      <div className="col-3">
        <div className="card text-left h-100 p-1">
          <img className="card-img-top" src={image} />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <h4 className="card-text">{price} $</h4>
          </div>
          <button
            onClick={() => {
              this.props.handleClick(this.props.data);
            }}
            className="btn btn-secondary"
          >
            Chi tiết
          </button>
          <button
            onClick={() => {
              this.props.handleAddToCart(this.props.data);
            }}
            className="btn btn-danger"
          >
            Chọn mua
          </button>
        </div>
      </div>
    );
  }
}
