import logo from "./logo.svg";
import "./App.css";
import ShoeShop from "./Bt_ShoeShop/ShoeShop";

function App() {
  return (
    <div className="App">
      <ShoeShop />
    </div>
  );
}

export default App;
